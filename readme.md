## Tic Tac Toe Game AI

Rust implementation of Tic Tac Toe Game with AI-competitor based on [minimax](https://en.wikipedia.org/wiki/Minimax) algorithm.

To run this game you have to have installed **cargo** tool with [rustup](https://rustup.rs/).
Then you can enter:
```
cargo run;
```
You can also build this game via command:
```
cargo build;
```