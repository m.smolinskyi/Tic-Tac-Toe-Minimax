use std::{cmp, io, usize};

use configs::{AI, HUMAN, SCORE_O, SCORE_TIE, SCORE_X};

use crate::display::print_board;

mod configs;
mod display;

fn best_move(board: &mut [[char; 3]; 3], current_player: &mut char) {
    let mut best_score = i32::MIN;
    let mut step: Option<(usize, usize)> = None;

    for i in 0..3 {
        for j in 0..3 {
            if board[i][j] == ' ' {
                board[i][j] = AI;
                let score = minimax(board, 0, false);
                board[i][j] = ' ';
                if score > best_score {
                    best_score = score;
                    step = Some((i, j));
                }
            }
        }
    }

    if let Some(step) = step {
        board[step.0][step.1] = AI;
        *current_player = HUMAN;
    }
}

fn minimax(board: &mut [[char; 3]; 3], depth: u32, is_maximizing: bool) -> i32 {
    let result = check_winner(&board);
    if result == AI {
        return SCORE_X;
    } else if result == HUMAN {
        return SCORE_O;
    } else if result == '=' {
        return SCORE_TIE;
    }

    if is_maximizing {
        let mut best_score = i32::MIN;

        for i in 0..3 {
            for j in 0..3 {
                if board[i][j] == ' ' {
                    board[i][j] = AI;
                    let score = minimax(board, depth + 1, false);
                    board[i][j] = ' ';
                    best_score = cmp::max(score, best_score);
                }
            }
        }
        return best_score;
    } else {
        let mut best_score = i32::MAX;
        for i in 0..3 {
            for j in 0..3 {
                if board[i][j] == ' ' {
                    board[i][j] = HUMAN;
                    let score = minimax(board, depth + 1, true);
                    board[i][j] = ' ';
                    best_score = cmp::min(score, best_score);
                }
            }
        }
        return best_score;
    }
}

fn ask_player_move(board: &[[char; 3]; 3]) -> Result<(usize, usize), String> {
    loop {
        println!("> Select your move in range 1..9: ");
        let mut input = String::new();
        io::stdin()
            .read_line(&mut input)
            .map_err(|_| "[!] Failed to read line".to_string())?;

        match input.trim().parse::<usize>() {
            Ok(player_move) => {
                if player_move < 1 || player_move > 9 {
                    println!("[!] Invalid: out of game board");
                    continue;
                }
                let (i, j) = get_indexes_by_move_number(player_move);
                if board[i][j] != ' ' {
                    println!("[!] Invalid: {} already filled", player_move);
                } else {
                    return Ok((i, j));
                }
            }
            Err(_) => println!("[!] Invalid: please enter a number"),
        }
    }
}

fn get_indexes_by_move_number(step: usize) -> (usize, usize) {
    let i: usize = (step - 1) / 3;
    let j: usize = (step - 1) % 3;
    (i, j)
}

fn check_winner(board: &[[char; 3]; 3]) -> char {
    let mut winner: char = '-';
    // horizontal
    for i in 0..3 {
        if equals3(board[i][0], board[i][1], board[i][2]) {
            winner = board[i][0];
        }
    }

    // Vertical
    for i in 0..3 {
        if equals3(board[0][i], board[1][i], board[2][i]) {
            winner = board[0][i];
        }
    }

    // Diagonal
    if equals3(board[0][0], board[1][1], board[2][2]) {
        winner = board[0][0];
    }
    if equals3(board[2][0], board[1][1], board[0][2]) {
        winner = board[2][0];
    }

    let mut open_spots = 0;
    for i in 0..3 {
        for j in 0..3 {
            if board[i][j] == ' ' {
                open_spots += 1;
            }
        }
    }

    if winner == '-' && open_spots == 0 {
        return '=';
    }

    winner
}

fn equals3(a: char, b: char, c: char) -> bool {
    return a == b && b == c && a != ' ';
}

fn main() {
    let mut current_player = HUMAN;

    let mut board: [[char; 3]; 3] = [[' ', ' ', ' '], [' ', ' ', ' '], [' ', ' ', ' ']];

    board[0][0] = AI;
    print_board(&board);

    loop {
        if current_player == HUMAN {
            let (i, j) = ask_player_move(&board).unwrap();

            board[i][j] = HUMAN;

            current_player = AI;
            best_move(&mut board, &mut current_player);
        }

        let result = check_winner(&board);
        print_board(&board);

        if result != '-' {
            if result == '=' {
                println!("> tie");
                break;
            } else {
                println!("> {} win!", result);
                break;
            }
        }
    }
}
