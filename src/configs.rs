pub const SCORE_X: i32 = 10;
pub const SCORE_O: i32 = -10;
pub const SCORE_TIE: i32 = 0;
pub const AI: char = 'X';
pub const HUMAN: char = 'O';
pub const DISPLAY_MATRIX: [[char; 3]; 3] = [
    ['1', '2', '3'],
    ['4', '5', '6'],
    ['7', '8', '9'],
];
