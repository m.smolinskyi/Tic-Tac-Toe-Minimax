use tabled::{builder::Builder, settings::Style};
use crate::configs::DISPLAY_MATRIX;

pub fn print_board(board: &[[char; 3]; 3]) {
    let mut builder = Builder::default();
    for i in 0..3 {
        let mut row: Vec<char> = Vec::new();
        for j in 0..3 {
            match board[i][j] {
                'X' => row.push('X'),
                'O' => row.push('O'),
                _ => row.push(DISPLAY_MATRIX[i][j])
            }
        }
        builder.push_record(row);
    }
    let table = builder.build().with(Style::modern()).to_string();
    println!("{}", table);
}
